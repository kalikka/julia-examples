module rdf
export gofr

function dist(x,y,box,invbox)
  dist=zero(eltype(x))
  for i in eachindex(x,y,box,invbox)
    dist+=(x[i]-y[i]-box[i]*round((x[i]-y[i])*invbox[i]))^2
  end
  dist=sqrt(dist)
end

function binnumber(r,low,high,nbins) # Works for low <= r < high
  binnumber=1+floor(Int,(r-low)*nbins/(high-low))
end

function bincenter(i,low,high,nbins)
  bincenter=Float64(low+(i-0.5)*((high-low)/nbins))
end

function gofr(fn,box,rmax,nbins)
if rmax > findmin(box)[1]/2.0
  rmax = findmin(box)[1]/2.0
  print("Requested Rmax too high, reducing to Rmax = box/2 = ",rmax,"\n")
end
invbox=zeros(Float64,3) # multiplication faster than division
for i in eachindex(box)
  invbox[i]=1.0/box[i]
end
cellvol=Float64(box[1]*box[2]*box[3])

nions=zero(Int)
framei=zero(Int)
g=zeros(Int64,nbins,5) # Using Int64 for headroom, 1st index is the histogram from 0 to rmax, 2nd index is the different g(r)'s (1=Ga-Ga, 2=Ga-Sb, 3=Sb-Ga, 4=Sb-Sb, 5=any to any)
f=open(fn,"r")
while true
  try # See if file continues, if it doesn't we'll get out
    nions=parse(Int,readline(f))
  catch
    break
  end
  framei+=1 # Assuming the file is intact now, if it's not it's on the user
  if !@isdefined x # Define coordinate array, if not already defined
    x=zeros(Float64, 3,nions) # Using Float64 for best accuracy
  end
  if !@isdefined labels # Define atomic label array, if not already defined
    labels=Array{String}(undef,nions)
  end
  readline(f) # Read, and discard, the comment line
  for i in 1:nions # Read coordinate lines
    coords=split(readline(f))
    labels[i]=coords[1] # Atomic labels
    x[1,i]=parse(Float64, coords[2]) # xyz coordinates
    x[2,i]=parse(Float64, coords[3])
    x[3,i]=parse(Float64, coords[4])
  end

  for i in 1:nions-1 
    for j in i+1:nions
      d=dist(x[:,i],x[:,j],box,invbox)
      if d >= rmax # "=" due to how binnumber works
        continue
      end
      bin=binnumber(d,Float64(0.0),rmax,nbins)
      # We went through each atom pair ij only once so we need to count both Ga-Sb and Sb-Ga g(r)s whenever we get Ga and Sb either way. But this leads Ga-Sb and Sb-Ga pairs getting double-counted wrt. Ga-Ga, Sb-Sb, and Any-Any pairs so we need to double-count those too
      if labels[i] == "Ga"
        if labels[j] == "Ga"
          g[bin,1]+=2
        elseif labels[j] == "Sb"
          g[bin,2]+=1
          g[bin,3]+=1
        else
          print("UNKNOWN ATOM PAIR: ",labels[i]," ",i,", ",labels[j]," ",j,"\n")
        end
      elseif labels[i] == "Sb"
        if labels[j] == "Ga"
          g[bin,2]+=1
          g[bin,3]+=1
        elseif labels[j] == "Sb"
          g[bin,4]+=2
        else
          print("UNKNOWN ATOM PAIR: ",labels[i]," ",i,", ",labels[j]," ",j,"\n")
        end
      else
        print("UNKNOWN ATOM PAIR: ",labels[i]," ",i,", ",labels[j]," ",j,"\n")
      end
      g[bin,5]+=2
    end
  end
  if framei == 1
    nga=zero(Int64)
    nsb=zero(Int64)
    for i in 1:nions
      if labels[i] == "Ga"
        nga+=1
      elseif labels[i] == "Sb"
        nsb+=1
      else
        print("UNKNOWN ATOM: ",labels[i]," ",i,"\n")
      end
    end
    global invnions=Float64(1.0/nions)
    global invnga=Float64(1.0/nga)
    global invnsb=Float64(1.0/nsb)
  end
end # end of file reading loop

# Two different normalizations, both seem to give correct result
if false # Normalize by the dV(r) = volume of sphere with radius R+dR minus volume of sphere with radius R
  inv3nbins3=Float64(1.0/(3.0*nbins*nbins*nbins))
  rmax3=Float64(rmax*rmax*rmax)

  shellvol=zeros(Float64,nbins) #r^3 = (i*rmax/nbins)^3
  for i in 1:nbins
    shellvol[i]=4.0*pi*rmax3*i*i*i*inv3nbins3 # 4 pi r^3 at the far edge of i'th bin
  end

  deltashellvol=zeros(Float64,nbins)
  deltashellvol[1]=shellvol[1] # V at upper edge minus V at origin (=0)
  for i in 2:nbins
    deltashellvol[i]= shellvol[i]-shellvol[i-1] # V at upper edge of i'th bin minus V at lower edge of i'th bin
  end

  norm=zeros(Float64,nbins)
  for i in 1:nbins
    norm[i]=cellvol/deltashellvol[i]
  end
end

if true # Normalize by the dV(r) = dR times the surface area of sphere with radius R
  invnbins3=Float64(1.0/(nbins*nbins*nbins))
  rmax3=Float64(rmax*rmax*rmax)

  shellvol=zeros(Float64,nbins) #r^2 dr = (i*rmax/nbins)^2 * rmax/nbins = i^2 * rmax^3 / (nbins^3)
  for i in 1:nbins
    shellvol[i]=4.0*pi*rmax3*i*i*invnbins3 # 4 pi r^3 at the far edge of i'th bin
  end

  norm=zeros(Float64,nbins)
  for i in 1:nbins
    norm[i]=cellvol/shellvol[i]
  end
end

# Normalization to account for number of ions: first divide by number of "central" atom, then the number of the "other" atom (this divided by cellvol results in the partial number density for in the RDF equation), and finally divide by the number of structures in trajectory
invnga2=Float64(invnga*invnga/framei)
invngasb=Float64(invnga*invnsb/framei)
invnsb2=Float64(invnsb*invnsb/framei)
invnions2=Float64(invnions*invnions/framei)

# normalization: gr = g/(shellvol*density) = g*cellvol/(shellvol*natoms) = g*norm*invnions
gr=zeros(Float64,nbins,5)
for i in 1:nbins
  gr[i,1]=g[i,1]*norm[i]*invnga2
  gr[i,2]=g[i,2]*norm[i]*invngasb
  gr[i,3]=g[i,3]*norm[i]*invngasb
  gr[i,4]=g[i,4]*norm[i]*invnsb2
  gr[i,5]=g[i,5]*norm[i]*invnions2
end

r=zeros(Float64,nbins)
for i in 1:nbins
  r[i]=bincenter(i,Float64(0.0),rmax,nbins)
end
close(f)
return r,gr
end
end
