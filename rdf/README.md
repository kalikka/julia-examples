Radial Distribution Function example.

Calculates g(r) of binary alloy atomic structure(s) in .xyz file.

Usage example in test.jl

```
inputs:
nbins = number of histogram bins in the resulting g(r)
maxr = maximum radius to which g(r) is calculated, limited by box size
box = 3-vector of simulation cell side lengths, only supports rectangular simulation cells
fn = name of the .xyz file, written for binary alloy of Ga and Sb atoms, edit code as needed
```

```
outputs:
r = nbins-vector of histogram bin centers
gr = nbins x 5 -array of the g(r) values for each histogram bin. gr[:,1] is Ga-Ga, gr[:,2] is Ga-Sb, gr[:,3] is Sb-Ga (for debugging/verification purposes), gr[:,4] is Ga-Ga, and gr[:,5] is Any-Any atom pairs.
```
